/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package binarios_clase_2;

/**
 *
 * @author German
 */
public class Suma_Almacenamientos {
    
    
   public static void main(String []aergs)
   {
   
       //Creacion de bytes de las cuatro operaciones
       Byte byt= new Byte();
       byt.Suma_Byte("1010001", "'000010");
       byt.Suma_Byte_Word("1010101", "101010101010101");
       byt.Suma_Byte_DWord("1010101", "10110010101010100101");
       byt.Suma_Byte_QWord("101010", "10101010010101010010101010101010101");
   //-----------------------------------------------------------------------
       
       System.out.println("----------------------------------------------------");
       System.out.println("");
       
       //Creacion de Word de las cuatro operaciones
       Word word= new Word();
       word.Suma_Byte_Word("1010001", "101001010101");
       word.Suma_Word("101010101", "101010101");
       word.Suma_Word_DWord("1010101000", "1001010101010101001");
       word.Suma_word_QWord("10101010", "110010101010101010101010101");
   //-----------------------------------------------------------------------
   
       System.out.println("----------------------------------------------------");
       System.out.println("");
       
        //Creacion de DWord de las cuatro operaciones
       
       Dword dword= new Dword();
       dword.Suma_Byte_DWord("100101", "1010101010101");
       dword.Suma_Word_DWord("100011", "100100010101010");
       dword.Suma_DWord("0101010101", "1010101010101001");
       dword.Suma_dword_QWord("1010101001", "101010101010010101001");
      
       //-----------------------------------------------------------------------
   
       System.out.println("----------------------------------------------------");
       System.out.println("");
       
       
        //Creacion de QWord de las cuatro operaciones
       Qword qword= new Qword();
       qword.Suma_Byte_QWord("10001", "10010101010101");
       qword.Suma_Word_QWord("1010101001", "1010101010101010");
       qword.Suma_Dword_QWord("101001010", "1010101010010101010");
       qword.Suma_Qword("10101001010101", "101010101001010101010");
   
   
   }
   
    
}
