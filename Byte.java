/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package binarios_clase_2;

/**
 *
 * @author German
 */
public class Byte {
     String Byt[];
     String Wor[];
     String Dwor[];
     String Qwor[];
    char d;
    public Byte(){
        
    Byt = new String[8];
    Wor= new String[16];
    Dwor= new String[32];
    Qwor= new String[64];
    
    
    
    }
       public  String Suma_Binaria(String binario1, String binario2) {
    if (binario1 == null || binario2== null) 
    {
        return "";
    }
    int primer_elemento =binario1.length() - 1;
    int segundo_elemento = binario2.length() - 1;
    StringBuilder sb = new StringBuilder();
    int acarreado = 0;
    while (primer_elemento >= 0 || segundo_elemento >= 0) {
        int sum = acarreado;
        if (primer_elemento >= 0) {
            sum += binario1.charAt(primer_elemento) - '0';
            primer_elemento--;
        }
        if (segundo_elemento >= 0) {
            sum +=binario2.charAt(segundo_elemento) - '0';
            segundo_elemento--;
        }
        acarreado = sum >> 1;
        sum = sum & 1;
        sb.append(sum == 0 ? '0' : '1');
    }
    if (acarreado > 0){
        
        sb.append('1');
    }

    sb.reverse();
    return String.valueOf(sb);
}    
    public void Suma_Byte(String tamaño,String tamaño2)
    {
        String resultado;
      //Almacenamiento_Enteros b = null;
   
    if(tamaño.length()<=8&&tamaño2.length()<=8)
    {
    System.out.println("La siguiente suma es entre dos Bytes");
    
    resultado=(Suma_Binaria(tamaño,tamaño2));
    System.out.println("Comprobando Almacenamiento : "+resultado.length());
    
    if(resultado.length()<=8)
    {
        for(int i=0;i<=resultado.length();i++)
        {
        Byt[i]="0";
       
        }
   
       for(int i=resultado.length();i<Byt.length;i++)
       {
           if(Byt[i]==null)
           {
       Byt[i]="0";
       }
       }
       for(int i=resultado.length();i<Byt.length;i++)
       {
       System.out.print(Byt[i]);
       }
        System.out.println(Suma_Binaria(tamaño,tamaño2));
        
    }else
    {
         System.out.println("Caso de desbordamiento");
    System.out.println("00000000");
    }
        
    }
        }
    

    public void Suma_Byte_Word(String Byte,String Word)
    {
        String resultado;
  //    Almacenamiento_Enteros b = null;
   
    if(Byte.length()<=8&&Word.length()<=16)
    {
    System.out.println("La siguiente suma es entre un Byte y un Word");
    
    resultado=(Suma_Binaria(Byte,Word));
    System.out.println("Comprobando Almacenamiento : "+resultado.length());
    
    if(resultado.length()<=16)
    {
        for(int i=0;i<=resultado.length();i++)
        {
        Wor[i]="0";
       
        }
   
       for(int i=resultado.length();i<Wor.length;i++)
       {
           if(Wor[i]==null)
           {
       Wor[i]="0";
       }
       }
       for(int i=resultado.length();i<Wor.length;i++)
       {
       System.out.print(Wor[i]);
       }
        System.out.println(Suma_Binaria(Byte,Word));
        
    }else
    {
         System.out.println("Caso de desbordamiento");
    System.out.println("0000000000000000");
    }
        
    }
        }
    
 public void Suma_Byte_DWord(String Byte,String DWord)
    {
        String resultado;
  //    Almacenamiento_Enteros b = null;
   
    if(Byte.length()<=8&&DWord.length()<=32)
    {
    System.out.println("La siguiente suma es entre un Byte y un DWord");
    
    resultado=(Suma_Binaria(Byte,DWord));
    System.out.println("Comprobando Almacenamiento : "+resultado.length());
    
    if(resultado.length()<=32)
    {
        for(int i=0;i<=resultado.length();i++)
        {
        Dwor[i]="0";
       
        }
   
       for(int i=resultado.length();i<Dwor.length;i++)
       {
           if(Dwor[i]==null)
           {
       Dwor[i]="0";
       }
       }
       for(int i=resultado.length();i<Dwor.length;i++)
       {
       System.out.print(Dwor[i]);
       }
        System.out.println(Suma_Binaria(Byte,DWord));
        
    }else
    {
         System.out.println("Caso de desbordamiento");
    System.out.println("00000000000000000000000000000000");
    }
        
    }
        }
    
 public void Suma_Byte_QWord(String Byte,String QWord)
    {
        String resultado;
  //    Almacenamiento_Enteros b = null;
   
    if(Byte.length()<=8&&QWord.length()<=64)
    {
    System.out.println("La siguiente suma es entre un Byte y un QWord");
    
    resultado=(Suma_Binaria(Byte,QWord));
    System.out.println("Comprobando Almacenamiento : "+resultado.length());
    
    if(resultado.length()<=64)
    {
        for(int i=0;i<=resultado.length();i++)
        {
        Qwor[i]="0";
       
        }
   
       for(int i=resultado.length();i<Qwor.length;i++)
       {
           if(Qwor[i]==null)
           {
       Qwor[i]="0";
       }
       }
       for(int i=resultado.length();i<Qwor.length;i++)
       {
       System.out.print(Qwor[i]);
       }
        System.out.println(Suma_Binaria(Byte,QWord));
        
    }else
    {
         System.out.println("Caso de desbordamiento");
    System.out.println("0000000000000000000000000000000000000000000000000000000000000000");
    }
        
    }
        }
    
    public static void main(String []Args)
    {
 
    Byte e= new Byte();
    e.Suma_Byte("1001", "1010");
    e.Suma_Byte_Word("1001010", "101010101");
    e.Suma_Byte_DWord("1010000", "10100101010");
    e.Suma_Byte_QWord("101010", "101010100101010101");
    }
       

}